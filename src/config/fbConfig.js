import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

//Initialize Firebase
var config = {
  apiKey: 'AIzaSyAIl8104cPg7XBoqjH_wSUmHVsh2nVe2Rs',
  authDomain: 'net-ninja-marioplan-7d419.firebaseapp.com',
  databaseURL: 'https://net-ninja-marioplan-7d419.firebaseio.com',
  projectId: 'net-ninja-marioplan-7d419',
  storageBucket: 'net-ninja-marioplan-7d419.appspot.com',
  messagingSenderId: '741024967563',
  appId: '1:741024967563:web:4acf31c6b24e86254e1d8e',
  measurementId: 'G-01XGP0NNGD',
};

firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;
